var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ClickerGame;
(function (ClickerGame) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.AUTO, 'content', null);
            this.state.add('Boot', ClickerGame.Boot, false);
            this.state.add('Preloader', ClickerGame.Preloader, false);
            this.state.add('Zone', ClickerGame.Zone, false);
            this.state.start('Boot');
        }
        return Game;
    }(Phaser.Game));
    ClickerGame.Game = Game;
})(ClickerGame || (ClickerGame = {}));
//# sourceMappingURL=App.js.map