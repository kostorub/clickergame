var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ClickerGame;
(function (ClickerGame) {
    var Zone = (function (_super) {
        __extends(Zone, _super);
        function Zone() {
            _super.apply(this, arguments);
        }
        Zone.prototype.create = function () {
            this.background = this.add.sprite(0, 0, 'Background');
        };
        return Zone;
    }(Phaser.State));
    ClickerGame.Zone = Zone;
})(ClickerGame || (ClickerGame = {}));
//# sourceMappingURL=Zone.js.map