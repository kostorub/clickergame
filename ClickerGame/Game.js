var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
window.onload = function () {
    var game = new ClickerGame.Game();
};
var ClickerGame;
(function (ClickerGame) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.load.image('PreloadBar', 'Assets/Sprites/ProgressBar.png');
        };
        Boot.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
                //  If you have any desktop specific settings, they can go in here
                this.stage.game.scale.pageAlignHorizontally = true;
            }
            else {
            }
            this.game.state.start('Preloader', true, false);
        };
        return Boot;
    }(Phaser.State));
    ClickerGame.Boot = Boot;
})(ClickerGame || (ClickerGame = {}));
var ClickerGame;
(function (ClickerGame) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 800, 600, Phaser.AUTO, 'content', null);
            this.state.add('Boot', ClickerGame.Boot, false);
            this.state.add('Preloader', ClickerGame.Preloader, false);
            this.state.add('Zone', ClickerGame.Zone, false);
            this.state.start('Boot');
        }
        return Game;
    }(Phaser.Game));
    ClickerGame.Game = Game;
})(ClickerGame || (ClickerGame = {}));
var ClickerGame;
(function (ClickerGame) {
    var Item = (function () {
        function Item() {
        }
        return Item;
    }());
    ClickerGame.Item = Item;
})(ClickerGame || (ClickerGame = {}));
var ClickerGame;
(function (ClickerGame) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
            this.pathToSprites = 'Assets/Sprites/';
        }
        Preloader.prototype.preload = function () {
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, 'ProgressBar');
            this.load.setPreloadSprite(this.preloadBar);
            //  Load our actual games assets
            this.load.image('Background', this.pathToSprites + 'Background.png');
            this.load.image('Items', this.pathToSprites + 'Bars.png');
            this.load.image('Craft', this.pathToSprites + 'BlastFurnace.png');
            this.load.image('CopperOre', this.pathToSprites + 'CopperOre.png');
            this.load.image('TinOre', this.pathToSprites + 'TinOre.png');
            this.load.image('SilverOre', this.pathToSprites + 'SilverOre.png');
        };
        Preloader.prototype.create = function () {
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
        };
        Preloader.prototype.startMainMenu = function () {
            this.game.state.start('Zone', true, false);
        };
        return Preloader;
    }(Phaser.State));
    ClickerGame.Preloader = Preloader;
})(ClickerGame || (ClickerGame = {}));
var ClickerGame;
(function (ClickerGame) {
    var Zone = (function (_super) {
        __extends(Zone, _super);
        function Zone() {
            _super.apply(this, arguments);
        }
        Zone.prototype.create = function () {
            this.game.stage.backgroundColor = 'rgb(200, 200, 200)';
            this.menuItems = this.add.sprite(10, 10, 'Items');
            this.menuItems.scale.setTo(0.3);
            this.menuCraft = this.add.sprite(10, 150, 'Craft');
            this.menuCraft.scale.setTo(0.3);
            this.copperOre = this.add.sprite(200, 10, 'CopperOre');
            this.copperOre.scale.setTo(0.2);
            this.tinOre = this.add.sprite(200, 110, 'TinOre');
            this.tinOre.scale.setTo(0.2);
            this.silverOre = this.add.sprite(200, 210, 'SilverOre');
            this.silverOre.scale.setTo(0.2);
            this.buttonExtract = this.add.button(this.game.world.width - 100, this.game.world.height - 50, 'button', this.mine, this, 2, 1, 0);
            this.menuItems.inputEnabled = true;
            this.menuItems.events.onInputDown.add(this.menuItemsClicked, this);
            this.menuCraft.inputEnabled = true;
            this.menuCraft.events.onInputDown.add(this.menuCraftClicked, this);
        };
        Zone.prototype.menuItemsClicked = function () {
            this.copperOre.visible = true;
            this.tinOre.visible = true;
            this.silverOre.visible = true;
        };
        Zone.prototype.menuCraftClicked = function () {
            this.copperOre.visible = false;
            this.tinOre.visible = false;
            this.silverOre.visible = false;
        };
        Zone.prototype.mine = function () {
        };
        return Zone;
    }(Phaser.State));
    ClickerGame.Zone = Zone;
})(ClickerGame || (ClickerGame = {}));
//# sourceMappingURL=Game.js.map