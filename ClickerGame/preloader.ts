﻿module ClickerGame {

    export class Preloader extends Phaser.State {

        preloadBar: Phaser.Sprite;
        pathToSprites: string = 'Assets/Sprites/';

        preload() {

            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, 'ProgressBar');
            this.load.setPreloadSprite(this.preloadBar);

            //  Load our actual games assets
            this.load.image('Background', this.pathToSprites + 'Background.png');
            this.load.image('Items', this.pathToSprites + 'Bars.png');
            this.load.image('Craft', this.pathToSprites + 'BlastFurnace.png');
            this.load.image('CopperOre', this.pathToSprites + 'CopperOre.png');
            this.load.image('TinOre', this.pathToSprites + 'TinOre.png');
            this.load.image('SilverOre', this.pathToSprites + 'SilverOre.png');
        }

        create() {

            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);

        }

        startMainMenu() {

            this.game.state.start('Zone', true, false);

        }

    }

}