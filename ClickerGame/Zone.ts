﻿module ClickerGame {

    export class Zone extends Phaser.State {

        background: Phaser.Sprite;
        menuItems: Phaser.Sprite;
        menuCraft: Phaser.Sprite;

        copperOre: Phaser.Sprite;
        tinOre: Phaser.Sprite;
        silverOre: Phaser.Sprite;

        buttonExtract: Phaser.Button;

        create() {
            this.game.stage.backgroundColor = 'rgb(200, 200, 200)';
            this.menuItems = this.add.sprite(10, 10, 'Items');
            this.menuItems.scale.setTo(0.3);
            this.menuCraft = this.add.sprite(10, 150, 'Craft');
            this.menuCraft.scale.setTo(0.3);

            this.copperOre = this.add.sprite(200, 10, 'CopperOre');
            this.copperOre.scale.setTo(0.2);
            this.tinOre = this.add.sprite(200, 110, 'TinOre');
            this.tinOre.scale.setTo(0.2);
            this.silverOre = this.add.sprite(200, 210, 'SilverOre');
            this.silverOre.scale.setTo(0.2);

            this.buttonExtract = this.add.button(this.game.world.width - 100, this.game.world.height - 50, 'button', this.mine, this, 2, 1, 0);

            this.menuItems.inputEnabled = true;
            this.menuItems.events.onInputDown.add(this.menuItemsClicked, this);

            this.menuCraft.inputEnabled = true;
            this.menuCraft.events.onInputDown.add(this.menuCraftClicked, this);
        }

        menuItemsClicked() {
            this.copperOre.visible = true;
            this.tinOre.visible = true;
            this.silverOre.visible = true;
        }

        menuCraftClicked() {
            this.copperOre.visible = false;
            this.tinOre.visible = false;
            this.silverOre.visible = false;
        }

        mine() {

        }
    }
}